# Game Of Life

[John Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life)

## Keybord input

### File
- <kbd>L</kbd>: load a file from rle folder
- <kbd>S</kbd>: save selection into a file in rle folder
- <kbd>escape</kbd>: quit

### Edition
- <kbd>W</kbd>: change user mode: Selection / Draw / Move
- <kbd>E</kbd>: empty selection
- <kbd>R</kbd>: rotate selection
- <kbd>T</kbd>: flip selection
- <kbd>Y</kbd>: mirror selection
- <kbd>V</kbd>: change border type solid / wrap
- <kbd>Z</kbd>: generate a random state

### Algorithm
- <kbd>C</kbd>: switch algorithm

### Nav
- <kbd>space</kbd>: pause
- <kbd>G</kbd>: reset speed
- <kbd>F</kbd>: increase speed
- <kbd>D</kbd>: decrease speed
- <kbd>&rarr;</kbd>: next step
- <kbd>&larr;</kbd>: previous step

### View
- <kbd>M</kbd>: enable / disable grid

### Debug
- <kbd>A</kbd>: print the current state in the console
- <kbd>I</kbd>: display performance in the console
- <kbd>U</kbd>: mode info

## Tools needed

- GNU compiler g++
- CMake or GNU Makefile

## Dependencies needed

- SDL2
- SDL2_ttf

## Build

- using CMAKE:

at root level:

```
mkdir build
cd build
cmake ..
make
```

launch with:
```
./GameOfLife
```

- using Makefile

at root level:

```
make
```

launch with:
```
./bin/program
```
