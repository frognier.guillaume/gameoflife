#include "gameOfLife.h"

GameOfLife::GameOfLife()
{
    m_sAppName = "GameOfLife";
}

GameOfLife::~GameOfLife()
{
    for (int i = 0; i < m_stateIndexSize; i++) {
        delete[] m_state[i];
        m_state[i] = nullptr;
    }
    delete[] m_state;
    m_state = nullptr;
}

bool GameOfLife::onUserCreate()
{
    m_delay = 100;
    m_shiftx = 0;
    m_shifty = 0;
    m_pixelSize = 1;
    m_width = 1000; // windowWidth()/m_pixelSize;
    m_height = 1000; // windowHeight()/m_pixelSize;
    m_stateIndexSize = 10;
    m_stateIndex = 0;
    m_state = new short *[m_stateIndexSize];
    for (int i = 0; i < m_stateIndexSize; i++)
        m_state[i] = new short[m_width * m_height];
    m_nCount = 0;
    for (int i = 0; i < m_stateIndexSize; i++)
        listoftwo(m_state[i], m_width * m_height);
    setBackGroundColor(127, 127, 127);
    m_GameState = GS_PAUSING;
    m_NextState = GS_PAUSING;
    m_UserMode = UM_DRAW;
    m_Algorithm = A_QUICK;
    m_Border = B_SOLID;
    m_showGrid = true;
    m_selectx1 = 0;
    m_selectx2 = 0;
    m_selecty1 = 0;
    m_selecty2 = 0;
    return true;
}

bool GameOfLife::onUserUpdate(float fElapsedTime)
{
    switch (m_GameState) {
    case (GS_PAUSING):
        show(fElapsedTime);
        m_NextState = GS_PAUSE;
        break;
    case (GS_PAUSE):
        m_NextState = GS_PAUSE;
        break;
    case (GS_STEP_BY_STEP):
        m_delay = 100;
        switch (m_Algorithm) {
        case A_QUICK:
            evolveQL();
            break;
        case A_REGULAR:
            evolveRL();
            break;
        }
        show(fElapsedTime);
        m_NextState = GS_PAUSE;
        break;
    case (GS_PLAY):
        switch (m_Algorithm) {
        case A_QUICK:
            evolveQL();
            break;
        case A_REGULAR:
            evolveRL();
            break;
        }
        show(fElapsedTime);
        m_NextState = GS_PLAY;
        break;
    case (GS_ASK_LOADING):
        show(fElapsedTime);
        drawString("File name:", 10, 10, 20, 255, 0, 0);
        if (load(askBox(10, 30, windowWidth() - 20, 30))) {
            m_nCount = 0;
        }
        m_NextState = GS_PAUSING;
        m_UserMode = UM_SELECT;
        break;
    case (GS_ASK_SAVING):
        drawString("File name:", 10, 10, 20, 255, 0, 0);
        if (save(askBox(10, 30, windowWidth() - 20, 30))) {
            m_nCount = 0;
        }
        m_NextState = GS_PAUSING;
        break;
    }
    m_GameState = m_NextState;
    handleEvents(fElapsedTime);
    SDL_Delay(m_delay);
    // std::cout << "cursor x,y= " << m_cursorx <<","<<m_cursory<<std::endl;
    if (m_cursorx <= 10 && m_cursorx > 0)
        m_shiftx += 10;
    else if (m_cursorx >= windowWidth() - 10 && m_cursorx < windowWidth() - 1)
        m_shiftx -= 10;
    if (m_cursory <= 10 && m_cursory > 0)
        m_shifty += 10;
    else if (m_cursory >= windowHeight() - 10 && m_cursory < windowHeight() - 1)
        m_shifty -= 10;
    if (m_GameState == GS_PAUSE)
        m_GameState = GS_PAUSING;
    /*
    if (m_shiftx > 2*m_width)
        m_shiftx = 2*m_width;
    else if (m_shiftx < -2*m_width)
        m_shiftx = -2*m_width;
    if (m_shifty > 2*m_height)
        m_shifty = 2*m_height;
    else if (m_shifty < -2*m_height)
        m_shifty = -2*m_height;
    */

    return true;
}

/*
 * states:
 * 0: dead cell near at least one alive cell (need to check next turn);
 * 1: cell alive;
 * 2: dead cell no need to check.
 *
 */
void GameOfLife::evolveQL()
{
    m_nCount++;
    int nextIndex = (m_stateIndex + 1) % m_stateIndexSize;
    listoftwo(m_state[nextIndex], m_width * m_height);
    for (int y = 0; y < m_height; y++) {
        for (int x = 0; x < m_width; x++) {
            if (m_state[m_stateIndex][y * m_width + x] != 2) {
                unsigned int neighbor = countNeighbor(m_stateIndex, x, y);
                if (m_state[m_stateIndex][y * m_width + x] == 1) {
                    if (neighbor < 2 || neighbor > 3) {
                        m_state[nextIndex][y * m_width + x] = 0;
                    } else {
                        m_state[nextIndex][y * m_width + x] = 1;
                    }
                } else if (m_state[m_stateIndex][y * m_width + x] == 0) {
                    if (neighbor == 3) {
                        m_state[nextIndex][y * m_width + x] = 1;
                        addNeighborhood(nextIndex, x, y);
                    } else if (!neighbor && !countNeighbor(nextIndex, x, y))
                        m_state[nextIndex][y * m_width + x] = 2;
                    else {
                        m_state[nextIndex][y * m_width + x] = 0;
                    }
                }
            }
        }
    }
    m_stateIndex = nextIndex;
}

void GameOfLife::evolveRL()
{
    m_nCount++;
    int nextIndex = (m_stateIndex + 1) % m_stateIndexSize;
    for (int y = 0; y < m_height; y++) {
        for (int x = 0; x < m_width; x++) {
            unsigned int neighbor = countNeighbor(m_stateIndex, x, y);
            if (m_state[m_stateIndex][y * m_width + x] == 1) {
                if (neighbor < 2 || neighbor > 3) {
                    m_state[nextIndex][y * m_width + x] = 0;
                } else {
                    m_state[nextIndex][y * m_width + x] = 1;
                }
            } else if (m_state[m_stateIndex][y * m_width + x] == 0) {
                if (neighbor == 3) {
                    m_state[nextIndex][y * m_width + x] = 1;
                } else {
                    m_state[nextIndex][y * m_width + x] = 0;
                }
            }
        }
    }
    m_stateIndex = nextIndex;
}

void GameOfLife::show(float fElapsedTime)
{
    clearScreen();
    fillRect(m_shiftx, m_shifty, m_width * m_pixelSize, m_height * m_pixelSize, 255, 255, 255);
    for (int x = 0; x < m_width; x++) {
        for (int y = 0; y < m_height; y++) {
            if (m_state[m_stateIndex][y * m_width + x] == 1)
                fillRect(x * m_pixelSize + m_shiftx, y * m_pixelSize + m_shifty, m_pixelSize, m_pixelSize, 0, 0, 0, 255);
            else if (m_state[m_stateIndex][y * m_width + x] == 0 && m_Algorithm == A_QUICK)
                fillRect(x * m_pixelSize + m_shiftx, y * m_pixelSize + m_shifty, m_pixelSize, m_pixelSize, 255, 165, 0, 255);
        }
    }
    if (m_showGrid)
        drawGrid(m_shiftx, m_shifty, m_width * m_pixelSize, m_height * m_pixelSize, m_pixelSize, m_pixelSize);
    // drawString(std::to_string(std::floor(1000/fElapsedTime))+"ev/s",12,16, 14, 255, 0, 255);
    drawString(std::to_string(m_nCount), 2, windowHeight() - 30, 15, 0, 0, 0);
    switch (m_GameState) {
    case GS_PLAY:
        drawString(std::to_string((int)std::round(1 / fElapsedTime)) + "FPS", 12, 16, 14, 255, 0, 255);
        if (m_delay > 100)
            drawString("slow " + std::to_string((m_delay - 100) / 10), 12, 2, 14, 255, 0, 255);
        else if (m_delay < 100)
            drawString("fast " + std::to_string((100 - m_delay) / 10), 12, 2, 14, 255, 0, 255);
        break;
    case GS_PAUSING:
        fillRect(2, 2, 4, 14, 255, 0, 0);
        fillRect(8, 2, 4, 14, 255, 0, 0);
        break;
    case GS_STEP_BY_STEP:
        fillRect(2, 2, 4, 14, 0, 0, 255);
        fillTriangle(8, 2, 8, 16, 10, 7, 0, 0, 255);
        break;
    default:
        break;
    }
    if (m_selectx2 && m_selecty2) {
        drawRect(
            m_selectx1 * m_pixelSize + m_shiftx,
            m_selecty1 * m_pixelSize + m_shifty,
            (m_selectx2 - m_selectx1) * m_pixelSize,
            (m_selecty2 - m_selecty1) * m_pixelSize,
            255,
            0,
            0,
            255);
    }
    switch (m_UserMode) {
    case (UM_DRAW):
        drawString("D", windowWidth() - 15, 5, 10, 255, 0, 0);
        break;
    case (UM_SELECT):
        drawString("S", windowWidth() - 15, 5, 10, 255, 0, 0);
        break;
    case (UM_MOVE):
        drawString("M", windowWidth() - 15, 5, 10, 255, 0, 0);
        break;
    case (UM_INFO):
        drawString("I", windowWidth() - 15, 5, 10, 255, 0, 0);
        break;
    }
}

void GameOfLife::populateCheckList()
{
    for (int y = 0; y < m_height; y++) {
        for (int x = 0; x < m_width; x++) {
            if (m_state[m_stateIndex][y * m_width + x] == 1) {
                addNeighborhood(m_stateIndex, x, y);
            }
        }
    }
}

void GameOfLife::addNeighborhood(int index, int x, int y)
{
    switch (m_Border) {
    case B_SOLID:
        for (int i = -1; i <= +1; i++) {
            if ((x + i) < 0 || (x + i) >= m_width)
                continue;
            for (int j = -1; j <= +1; j++) {
                if ((y + j) < 0 || (y + j) >= m_height)
                    continue;
                else if (m_state[index][(y + j) * m_width + (x + i)] == 2)
                    m_state[index][(y + j) * m_width + (x + i)] = 0;
            }
        }
        break;
    case B_WRAP:
        for (int i = -1; i <= +1; i++) {
            for (int j = -1; j <= +1; j++) {
                if (m_state[index][(y + j + m_height) % m_height * m_width + (x + i + m_width) % m_width] == 2)
                    m_state[index][(y + j + m_height) % m_height * m_width + (x + i + m_width) % m_width] = 0;
            }
        }
        break;
    }
}

void GameOfLife::handleEvents(float fElapsedTime)
{
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
        switch (event.type) {
        case SDL_QUIT:
            end();
            break;
        case SDL_KEYDOWN:
            switch (event.key.keysym.sym) {
            case SDLK_ESCAPE: // Shut down
                end();
                break;
            case SDLK_SPACE: // Pause
                if (m_GameState == GS_PLAY)
                    m_GameState = GS_PAUSING;
                else
                    m_GameState = GS_PLAY;
                break;
            case SDLK_RIGHT: // Next step
                m_GameState = GS_STEP_BY_STEP;
                break;
            case SDLK_LEFT: // Previous step
                if (m_stateIndex != m_stateIndex + 1 && m_nCount > 0) {
                    m_stateIndex = (m_stateIndex - 1 + m_stateIndexSize) % m_stateIndexSize;
                    m_nCount--;
                    m_GameState = GS_PAUSING;
                }
                break;
            case SDLK_a: // Display current state on the console
                printMatrix(m_state[m_stateIndex], m_width, m_height);
                break;
            case SDLK_c: // Switch Algorithm
                switch (m_Algorithm) {
                case (A_QUICK):
                    m_Algorithm = A_REGULAR;
                    twointozero(m_state[m_stateIndex], m_width * m_height);
                    if (m_GameState == GS_PAUSE)
                        m_GameState = GS_PAUSING;
                    break;
                case (A_REGULAR):
                    m_Algorithm = A_QUICK;
                    zerointotwo(m_state[m_stateIndex], m_width * m_height);
                    populateCheckList();
                    if (m_GameState == GS_PAUSE)
                        m_GameState = GS_PAUSING;
                    break;
                }
                break;
            case SDLK_d: // Decrease the speed
                m_delay += 10;
                if (m_delay > 5000)
                    m_delay = 5000;
                break;
            case SDLK_e: // Empty a part or the totality of the state
                switch (m_UserMode) {
                case UM_SELECT:
                case UM_MOVE:
                    erase(m_selectx1, m_selectx2, m_selecty1, m_selecty2);
                    break;
                case UM_DRAW:
                case UM_INFO:
                    erase(0, m_width, 0, m_height);
                    break;
                }
                m_nCount = 0;
                if (m_GameState == GS_PAUSE)
                    m_GameState = GS_PAUSING;
                break;
            case SDLK_f: // Faster speed
                m_delay -= 10;
                if (m_delay < 0)
                    m_delay = 0;
                break;
            case SDLK_g: // Reset speed
                m_delay = 100;
                break;
            case SDLK_i: // Display performance on the console
                std::cout << "Time/frame:" << fElapsedTime * 1000 << "ms" << std::endl;
                break;
            case SDLK_l: // Load a rle file
                m_GameState = GS_ASK_LOADING;
                break;
            case SDLK_m: // Matrix (Grid) Enable/Disable
                m_showGrid = !m_showGrid;
                if (m_GameState == GS_PAUSE)
                    m_GameState = GS_PAUSING;
                break;
            case SDLK_r: // Rotate a part of the state
                if (m_UserMode == UM_SELECT && m_selectx2) {
                    rotate();
                    if (m_GameState == GS_PAUSE)
                        m_GameState = GS_PAUSING;
                }
                break;
            case SDLK_s: // Save part of the state into a rle file
                m_GameState = GS_ASK_SAVING;
                break;
            case SDLK_t: // Flip
                if (m_UserMode == UM_SELECT && m_selectx2) {
                    flip();
                    if (m_GameState == GS_PAUSE)
                        m_GameState = GS_PAUSING;
                }
                break;
            case SDLK_u: // User mode info (debug)
                m_UserMode = ((m_UserMode == UM_INFO) ? UM_DRAW : UM_INFO);
                if (m_GameState == GS_PAUSE)
                    m_GameState = GS_PAUSING;
                break;
            case SDLK_v: // Change border type
                m_Border = ((m_Border == B_SOLID) ? B_WRAP : B_SOLID);
                if (m_GameState == GS_PAUSE)
                    m_GameState = GS_PAUSING;
                break;
            case SDLK_w: // change between user mode
                switch (m_UserMode) {
                case UM_DRAW:
                    m_UserMode = UM_SELECT;
                    break;
                case UM_INFO:
                case UM_SELECT:
                    m_selectx1 = 0;
                    m_selectx2 = 0;
                    m_selecty1 = 0;
                    m_selecty2 = 0;
                    m_UserMode = UM_DRAW;
                    break;
                case UM_MOVE:
                    m_UserMode = UM_SELECT;
                    break;
                }
                if (m_GameState == GS_PAUSE)
                    m_GameState = GS_PAUSING;
                break;
            case SDLK_y: // Mirror
                if (m_UserMode == UM_SELECT && m_selectx2) {
                    mirror();
                    if (m_GameState == GS_PAUSE)
                        m_GameState = GS_PAUSING;
                }
                break;
            case SDLK_z: // Random state
                random();
                m_nCount = 0;
                m_GameState = GS_PAUSING;
                break;
            default:
                break;
            }
            break;
        case SDL_MOUSEBUTTONDOWN:
            if (event.button.button == SDL_BUTTON_LEFT) {
                int x = event.button.x;
                int y = event.button.y;

                if (x >= m_shiftx && x < m_shiftx + m_width * m_pixelSize && y >= m_shifty && y < m_shifty + m_height * m_pixelSize) {
                    x = (x - m_shiftx) / m_pixelSize;
                    y = (y - m_shifty) / m_pixelSize;
                    switch (m_UserMode) {
                    case UM_DRAW:
                        m_state[m_stateIndex][y * m_width + x] = (m_state[m_stateIndex][y * m_width + x] + 1) % 2;
                        if (m_Algorithm == A_QUICK)
                            addNeighborhood(m_stateIndex, x, y);
                        if (m_GameState == GS_PAUSE)
                            m_GameState = GS_PAUSING;
                        break;
                    case UM_SELECT:
                    case UM_MOVE:
                        if (x >= m_selectx1 && x <= m_selectx2 && y >= m_selecty1 && y <= m_selecty2) {
                            m_movex = x;
                            m_movey = y;
                            m_UserMode = UM_MOVE;
                            if (m_GameState == GS_PAUSE)
                                m_GameState = GS_PAUSING;
                        } else {
                            m_movex = 0;
                            m_movey = 0;
                            m_selectx1 = x;
                            m_selecty1 = y;
                            m_selectx2 = 0;
                            m_selecty2 = 0;
                        }
                        // m_UserMode=UM_SELECT;
                        break;
                    case UM_INFO:
                        std::cout << "x,y=" << x << "," << y;
                        std::cout << " winx,winy=" << m_cursorx << "," << m_cursory << std::endl;
                        std::cout << "          current previous" << std::endl;
                        std::cout << "state:      " << m_state[m_stateIndex][y * m_width + x] << "        "
                                  << m_state[(m_stateIndex - 1 + m_stateIndexSize) % m_stateIndexSize][y * m_width + x] << std::endl;
                        std::cout << "neighbors:  " << countNeighbor(m_stateIndex, x, y) << "        "
                                  << countNeighbor((m_stateIndex - 1 + m_stateIndexSize) % m_stateIndexSize, x, y) << std::endl;
                        break;
                    }
                }
            } else if (event.button.button == SDL_BUTTON_MIDDLE) {
                m_shiftx = 0;
                m_shifty = 0;
                m_pixelSize = 1;
                if (m_GameState == GS_PAUSE)
                    m_GameState = GS_PAUSING;
            } else if (event.button.button == SDL_BUTTON_RIGHT) {
                m_selectx1 = 0;
                m_selectx2 = 0;
                m_selecty1 = 0;
                m_selecty2 = 0;
            }
            break;
        case SDL_MOUSEBUTTONUP:
            if (event.button.button == SDL_BUTTON_LEFT) {
                int x = event.button.x;
                int y = event.button.y;

                if (x >= m_shiftx && x < m_shiftx + m_width * m_pixelSize && y >= m_shifty && y < m_shifty + m_height * m_pixelSize) {
                    switch (m_UserMode) {
                    case UM_DRAW:
                    case UM_INFO:
                        break;
                    case UM_SELECT:
                        x = (x - m_shiftx) / m_pixelSize;
                        y = (y - m_shifty) / m_pixelSize;
                        if (x != m_selectx1 && y != m_selecty1) {
                            if (m_selectx1 > x) {
                                m_selectx2 = m_selectx1;
                                m_selectx1 = x;
                            } else
                                m_selectx2 = x;
                            if (m_selecty1 > y) {
                                m_selecty2 = m_selecty1;
                                m_selecty1 = y;
                            } else
                                m_selecty2 = y;
                        }
                        if (m_GameState == GS_PAUSE)
                            m_GameState = GS_PAUSING;
                        break;
                    case UM_MOVE:
                        move((x - m_shiftx) / m_pixelSize - m_movex + m_selectx1, (y - m_shifty) / m_pixelSize - m_movey + m_selecty1);
                        if (m_GameState == GS_PAUSE)
                            m_GameState = GS_PAUSING;
                        m_UserMode = UM_SELECT;
                        break;
                    }
                }
            }
            break;
        case SDL_MOUSEMOTION:
            m_cursorx = event.button.x;
            m_cursory = event.button.y;
            break;
        case SDL_MOUSEWHEEL:
            if (event.wheel.y > 0 && m_pixelSize < 100) { // scroll up
                m_shiftx += std::floor((float)(m_cursorx - m_shiftx) * (1 - (float)(m_pixelSize + 1) / m_pixelSize));
                m_shifty += std::floor((float)(m_cursory - m_shifty) * (1 - (float)(m_pixelSize + 1) / m_pixelSize));
                m_pixelSize++;
                if (m_GameState == GS_PAUSE)
                    m_GameState = GS_PAUSING;
            } else if (event.wheel.y < 0 && m_pixelSize > 1) { // scroll down
                m_shiftx -= std::floor((float)(m_cursorx - m_shiftx) * (1 - (float)(m_pixelSize + 1) / m_pixelSize));
                m_shifty -= std::floor((float)(m_cursory - m_shifty) * (1 - (float)(m_pixelSize + 1) / m_pixelSize));
                m_pixelSize--;
                if (m_GameState == GS_PAUSE)
                    m_GameState = GS_PAUSING;
            }
            break;
        default:
            break;
        }
    }
}

bool GameOfLife::save(std::string filename)
{
    std::fstream file;
    file.open(("./rle/" + filename + ".rle").c_str(), std::ios::out);
    if (!file.is_open()) {
        std::cout << "Save failed." << std::endl;
        return false;
    }

    unsigned int firstrow, lastrow, firstcol, lastcol;
    goto find_first_row;

find_first_row:
    for (int y = 0; y < m_height; y++) {
        for (int x = 0; x < m_width; x++) {
            if (m_state[m_stateIndex][y * m_width + x] == 1) {
                firstrow = y;
                goto find_last_row;
            }
        }
    }
find_last_row:
    for (int y = m_height - 1; y >= 0; y--) {
        for (int x = 0; x < m_width; x++) {
            if (m_state[m_stateIndex][y * m_width + x] == 1) {
                lastrow = y;
                goto find_first_col;
            }
        }
    }
find_first_col:
    for (int x = 0; x < m_width; x++) {
        for (int y = 0; y < m_height; y++) {
            if (m_state[m_stateIndex][y * m_width + x] == 1) {
                firstcol = x;
                goto find_last_col;
            }
        }
    }
find_last_col:
    for (int x = m_width - 1; x >= 0; x--) {
        for (int y = 0; y < m_height; y++) {
            if (m_state[m_stateIndex][y * m_width + x] == 1) {
                lastcol = x;
                goto end_save;
            }
        }
    }
end_save:

    file << "#O GF" << std::endl;
    std::string sFile;
    unsigned int no = 0; // o:alive=1
    unsigned int nb = 0; // b:dead=0,2
    unsigned int nEndl = 0;
    for (unsigned int y = firstrow; y <= lastrow; y++) {
        for (unsigned int x = firstcol; x <= lastcol; x++) {
            if (m_state[m_stateIndex][y * m_width + x] != 1) {
                nb++;
                if (no) {
                    if (no > 1)
                        sFile.append(std::to_string(no));
                    sFile.push_back('o');
                    no = 0;
                }
            } else if (m_state[m_stateIndex][y * m_width + x] == 1) {
                no++;
                if (nEndl) {
                    if (nEndl > 1)
                        sFile.append(std::to_string(nEndl));
                    sFile.push_back('$');
                    nEndl = 0;
                }
                if (nb) {
                    if (nb > 1)
                        sFile.append(std::to_string(nb));
                    sFile.push_back('b');
                    nb = 0;
                }
            }
        }
        if (no) {
            if (no > 1)
                sFile.append(std::to_string(no));
            sFile.push_back('o');
        }
        nEndl++;
        nb = 0;
        no = 0;
        if (sFile.size() > 10) {
            file << sFile;
            sFile.clear();
        }
    }
    sFile.push_back('!');
    file << sFile << std::endl;
    sFile.clear();

    m_selectx1 = firstcol;
    m_selectx2 = lastcol + 1;
    m_selecty1 = firstrow;
    m_selecty2 = lastrow + 1;

    std::cout << "Save done." << std::endl;
    file.close();
    return true;
}

bool GameOfLife::load(std::string filename)
{
    std::ifstream file;
    file.open(("./rle/" + filename + ".rle").c_str(), std::ios::in);
    if (!file.is_open()) {
        std::cout << "Load failed." << std::endl;
        return false;
    }
    unsigned int row = m_selecty1;
    unsigned int col = 0;
    std::string line;
    int n = 1;
    int k = m_selectx1;
    while (getline(file, line)) {
        if (line.front() != '#' && line.front() != 'x') {
            for (unsigned int i = 0; i < line.size(); i++) {
                // std::cout << line.at(i);
                if (line.at(i) == 'b') {
                    for (int j = 0; j < n; j++) {
                        m_state[m_stateIndex][row * m_width + k] = (m_Algorithm == A_QUICK ? 2 : 0);
                        // m_state[m_stateIndex][row*m_width+k]=0;
                        k++;
                        // std::cout << 0;
                    }
                    n = 1;
                } else if (line.at(i) == 'o') {
                    for (int j = 0; j < n; j++) {
                        m_state[m_stateIndex][row * m_width + k] = 1;
                        // if (m_Algorithm==A_QUICK) addNeighborhood(m_stateIndex,k,row);
                        k++;
                        // std::cout << 1;
                    }
                    n = 1;
                } else if (line.at(i) == '$') {
                    for (int j = 0; j < n; j++) {
                        row++;
                    }
                    n = 1;
                    if (k > (int)col)
                        col = k;
                    k = m_selectx1;
                    // std::cout << std::endl;
                } else if (line.at(i) == '!') {
                    // std::cout << std::endl;
                    break;
                } else {
                    n = line.at(i) - '0';
                    while (line.at(i + 1) - '0' >= 0 && line.at(i + 1) - '0' <= 9) {
                        n = n * 10 + line.at(i + 1) - '0';
                        i++;
                    }
                }
                if (n >= m_width - 1 || (int)row >= m_height - 1) {
                    std::cout << "Load fail: lack of space" << std::endl;
                    return false;
                }
            }
        } else if (line.front() == '#') {
            line.erase(line.begin(), line.begin() + 3);
            std::cout << line << std::endl;
        }
    }
    if (m_Algorithm == A_QUICK)
        populateCheckList();
    m_selectx2 = col;
    m_selecty2 = row + 1;

    std::cout << "Load done." << std::endl;
    file.close();
    return true;
}

void GameOfLife::rotate()
{
    int wi = m_selectx2 - m_selectx1;
    int hi = m_selecty2 - m_selecty1;
    int tempM[wi * hi];
    int i = 0;
    for (int y = m_selecty1; y < m_selecty2; y++) {
        for (int x = m_selectx1; x < m_selectx2; x++) {
            tempM[i] = m_state[m_stateIndex][y * m_width + x];
            m_state[m_stateIndex][y * m_width + x] = 2;
            i++;
        }
    }
    i = 0;
    for (int y = m_selecty1; y < m_selecty1 + wi; y++) {
        for (int x = m_selectx1; x < m_selectx1 + hi; x++) {
            // m_state[x*m_width+y] = tempM[wi*(hi-(i%hi+1))+i/hi];
            if (tempM[wi * (hi - (i % hi + 1)) + i / hi] == 1) {
                m_state[m_stateIndex][y * m_width + x] = 1;
                if (m_Algorithm == A_QUICK)
                    addNeighborhood(m_stateIndex, x, y);
            }
            i++;
        }
    }
}

void GameOfLife::erase(int xs, int xe, int ys, int ye)
{
    if (xe == 0)
        xe = m_width;
    if (ye == 0)
        ye = m_height;
    for (int y = ys; y < ye; y++) {
        for (int x = xs; x < xe; x++) {
            m_state[m_stateIndex][y * m_width + x] = (m_Algorithm == A_QUICK ? 2 : 0);
        }
    }
    if (m_Algorithm == A_QUICK)
        populateCheckList();
}

void GameOfLife::flip()
{
    int wi = m_selectx2 - m_selectx1;
    int hi = m_selecty2 - m_selecty1;
    int tempM[wi * hi];
    int i = 0;
    for (int y = m_selecty1; y < m_selecty2; y++) {
        for (int x = m_selectx1; x < m_selectx2; x++) {
            tempM[i] = m_state[m_stateIndex][y * m_width + x];
            m_state[m_stateIndex][y * m_width + x] = 2;
            i++;
        }
    }
    i = 0;
    for (int y = m_selecty1; y < m_selecty2; y++) {
        for (int x = m_selectx1; x < m_selectx2; x++) {
            if (tempM[(hi - (i / wi + 1)) * wi + i % wi] == 1) {
                m_state[m_stateIndex][y * m_width + x] = 1;
                if (m_Algorithm == A_QUICK)
                    addNeighborhood(m_stateIndex, x, y);
            }
            i++;
        }
    }
}

void GameOfLife::mirror()
{
    int wi = m_selectx2 - m_selectx1;
    int hi = m_selecty2 - m_selecty1;
    int tempM[wi * hi];
    int i = 0;
    for (int y = m_selecty1; y < m_selecty2; y++) {
        for (int x = m_selectx1; x < m_selectx2; x++) {
            tempM[i] = m_state[m_stateIndex][y * m_width + x];
            m_state[m_stateIndex][y * m_width + x] = 2;
            i++;
        }
    }
    i = 0;
    for (int y = m_selecty1; y < m_selecty2; y++) {
        for (int x = m_selectx1; x < m_selectx2; x++) {
            if (tempM[(i / wi + 1) * wi + (wi - 1 - i) % wi] == 1) {
                m_state[m_stateIndex][y * m_width + x] = 1;
                if (m_Algorithm == A_QUICK)
                    addNeighborhood(m_stateIndex, x, y);
            }
            i++;
        }
    }
}

void GameOfLife::move(int movex, int movey)
{
    int wi = m_selectx2 - m_selectx1;
    int hi = m_selecty2 - m_selecty1;
    int tempM[wi * hi];
    int i = 0;
    int prstateIndex = (m_stateIndex - 1 + m_stateIndexSize) % m_stateIndexSize;
    for (int y = m_selecty1; y < m_selecty2; y++) {
        for (int x = m_selectx1; x < m_selectx2; x++) {
            tempM[i] = m_state[m_stateIndex][y * m_width + x];
            m_state[m_stateIndex][y * m_width + x] = 0;
            m_state[prstateIndex][y * m_width + x] = 0;
            /*
            for (int i=0; i<m_stateIndexSize; i++)
        m_state[i][y*m_width+x] = 0;
            */
            i++;
        }
    }
    i = 0;
    if (movex < 0)
        movex = 0;
    else if (movex + wi > m_width)
        movex = m_width - wi;
    if (movey < 0)
        movey = 0;
    else if (movey + hi > m_height)
        movey = m_height - hi;

    for (int y = movey; y < movey + hi; y++) {
        for (int x = movex; x < movex + wi; x++) {
            m_state[m_stateIndex][y * m_width + x] = tempM[hi * (i / hi) + (i % hi)];
            i++;
        }
    }
    if (m_Algorithm == A_QUICK) {
        for (int x = movex; x < movex + wi; x++) {
            if (m_state[m_stateIndex][movey * m_width + x] == 1)
                addNeighborhood(m_stateIndex, x, movey);
            if (m_state[m_stateIndex][(movey + hi - 1) * m_width + x] == 1)
                addNeighborhood(m_stateIndex, x, movey + hi - 1);
        }
        for (int y = movey; y < movey + hi; y++) {
            if (m_state[m_stateIndex][y * m_width + movex] == 1)
                addNeighborhood(m_stateIndex, movex, y);
            if (m_state[m_stateIndex][y * m_width + movex + wi - 1] == 1)
                addNeighborhood(m_stateIndex, movex + wi - 1, y);
        }
    }

    m_selectx1 = movex;
    m_selectx2 = movex + wi;
    m_selecty1 = movey;
    m_selecty2 = movey + hi;
}

void GameOfLife::random()
{
    listofzero(m_state[m_stateIndex], m_width * m_height);
    for (int y = 0; y < m_height; y++) {
        for (int x = 0; x < m_width; x++) {
            srand48(time(NULL) + x + y);
            if (rand() % 2 == 0) {
                m_state[m_stateIndex][y * m_width + x] = 1;
                // if (m_Algorithm==A_QUICK) addNeighborhood(m_stateIndex,x,y);
            }
        }
    }
}

unsigned int GameOfLife::countNeighbor(int index, int x, int y)
{
    switch (m_Border) {
    case B_WRAP:
        return m_state[index][((y - 1 + m_height) % m_height) * m_width + ((x - 1 + m_width) % m_width)] % 2
            + m_state[index][(y % m_height) * m_width + ((x - 1 + m_width) % m_width)] % 2
            + m_state[index][((y + 1) % m_height) * m_width + ((x - 1 + m_width) % m_width)] % 2
            + m_state[index][((y - 1 + m_height) % m_height) * m_width + (x % m_width)] % 2 + m_state[index][((y + 1) % m_height) * m_width + (x % m_width)] % 2
            + m_state[index][((y - 1 + m_height) % m_height) * m_width + (x + 1) % m_width] % 2
            + m_state[index][(y % m_height) * m_width + (x + 1) % m_width] % 2 + m_state[index][((y + 1) % m_height) * m_width + (x + 1) % m_width] % 2;
    case B_SOLID:
        if (x == 0 && y == 0)
            return m_state[index][(y + 1) * m_width + (x)] % 2 + m_state[index][(y)*m_width + (x + 1)] % 2 + m_state[index][(y + 1) * m_width + (x + 1)] % 2;
        else if (x == 0 && y == m_height - 1)
            return m_state[index][(y - 1) * m_width + (x)] % 2 + m_state[index][(y - 1) * m_width + (x + 1)] % 2 + m_state[index][(y)*m_width + (x + 1)] % 2;
        else if (x == m_width - 1 && y == 0)
            return m_state[index][(y)*m_width + (x - 1)] % 2 + m_state[index][(y + 1) * m_width + (x - 1)] % 2 + m_state[index][(y + 1) * m_width + (x)] % 2;
        else if (x == m_width - 1 && y == m_height - 1)
            return m_state[index][(y - 1) * m_width + (x - 1)] % 2 + m_state[index][(y)*m_width + (x - 1)] % 2 + m_state[index][(y - 1) * m_width + (x)] % 2;
        else if (x == 0)
            return m_state[index][(y - 1) * m_width + (x)] % 2 + m_state[index][(y + 1) * m_width + (x)] % 2 + m_state[index][(y - 1) * m_width + (x + 1)] % 2
                + m_state[index][(y)*m_width + (x + 1)] % 2 + m_state[index][(y + 1) * m_width + (x + 1)] % 2;
        else if (x == m_width - 1)
            return m_state[index][(y - 1) * m_width + (x - 1)] % 2 + m_state[index][(y)*m_width + (x - 1)] % 2 + m_state[index][(y + 1) * m_width + (x - 1)] % 2
                + m_state[index][(y - 1) * m_width + (x)] % 2 + m_state[index][(y + 1) * m_width + (x)] % 2;
        else if (y == 0)
            return m_state[index][(y)*m_width + (x - 1)] % 2 + m_state[index][(y + 1) * m_width + (x - 1)] % 2 + m_state[index][(y + 1) * m_width + (x)] % 2
                + m_state[index][(y)*m_width + (x + 1)] % 2 + m_state[index][(y + 1) * m_width + (x + 1)] % 2;
        else if (y == m_height - 1)
            return m_state[index][(y - 1) * m_width + (x - 1)] % 2 + m_state[index][(y)*m_width + (x - 1)] % 2 + m_state[index][(y - 1) * m_width + (x)] % 2
                + m_state[index][(y - 1) * m_width + (x + 1)] % 2 + m_state[index][(y)*m_width + (x + 1)] % 2;
        else
            return m_state[index][(y - 1) * m_width + (x - 1)] % 2 + m_state[index][(y)*m_width + (x - 1)] % 2 + m_state[index][(y + 1) * m_width + (x - 1)] % 2
                + m_state[index][(y - 1) * m_width + (x)] % 2 + m_state[index][(y + 1) * m_width + (x)] % 2 + m_state[index][(y - 1) * m_width + (x + 1)] % 2
                + m_state[index][(y)*m_width + (x + 1)] % 2 + m_state[index][(y + 1) * m_width + (x + 1)] % 2;
    default:
        return 0;
    }
}

void printMatrix(short *input, unsigned int width, unsigned int height)
{
    for (unsigned int y = 0; y < height; y++) {
        for (unsigned int x = 0; x < width; x++) {
            std::cout << input[y * width + x];
        }
        std::cout << std::endl;
    }
}

void twointozero(short *output, unsigned int size)
{
    for (unsigned int i = 0; i < size; i++) {
        if (output[i] == 2)
            output[i] = 0;
    }
}

void zerointotwo(short *output, unsigned int size)
{
    for (unsigned int i = 0; i < size; i++) {
        if (output[i] == 0)
            output[i] = 2;
    }
}

void listofzero(short *output, unsigned int size)
{
    for (unsigned int i = 0; i < size; i++)
        output[i] = 0;
}

void listoftwo(short *output, unsigned int size)
{
    for (unsigned int i = 0; i < size; i++)
        output[i] = 2;
}
