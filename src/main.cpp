#include "gameOfLife.h"
#include <iostream>

int main()
{
    GameOfLife game;
    game.initWindow(1000, 1000);
    game.initText();
    game.start();
    return 0;
}
