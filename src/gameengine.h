#ifndef GAME_H
#define GAME_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <atomic>
#include <condition_variable>
#include <iostream>
#include <string>
#include <thread>

class GameEngine
{
public:
    // Constructors & Destructors
    GameEngine();
    virtual ~GameEngine();

    void initWindow(int width, int height, bool fullscreen = false);
    void initText();
    void setWindowTitle(unsigned fps);
    void setWindowTitle();

    virtual void handleEvents();
    void setBackGroundColor(Uint8 r, Uint8 g, Uint8 b);
    void render();
    void clearScreen();
    void clean();

    bool running();

    void drawString(std::string sSentence, int x, int y, int size = 15, Uint8 r = 0, Uint8 g = 0, Uint8 b = 0, Uint8 a = SDL_ALPHA_OPAQUE);
    void fillRect(int x, int y, int w, int h, Uint8 r, Uint8 g, Uint8 b, Uint8 a = SDL_ALPHA_OPAQUE);
    void drawRect(int x, int y, int w, int h, Uint8 r, Uint8 g, Uint8 b, Uint8 a = SDL_ALPHA_OPAQUE);
    void fillTriangle(int xa, int ya, int xb, int yb, int xc, int yc, Uint8 r, Uint8 g, Uint8 b, Uint8 a = SDL_ALPHA_OPAQUE);
    void drawTriangle(int xa, int ya, int xb, int yb, int xc, int yc, Uint8 r, Uint8 g, Uint8 b, Uint8 a = SDL_ALPHA_OPAQUE);
    void drawGrid(int xs, int ys, int w, int h, int sizex, int sizey, Uint8 r = 0, Uint8 g = 0, Uint8 b = 0, Uint8 a = SDL_ALPHA_OPAQUE);
    std::string askBox(int x, int y, int w, int h);

    void start();
    void GameThread();
    void end();

    virtual bool onUserCreate() = 0;
    virtual bool onUserUpdate(float fElapsedTime) = 0;

    int windowWidth() const;
    int windowHeight() const;

protected:
    std::string m_sAppName;

private:
    int m_windowWidth;
    int m_windowHeight;
    bool m_bFont;
    bool m_bRenderNeeded;
    SDL_Window *m_window;
    SDL_Renderer *m_renderer;
    TTF_Font *m_font;
    std::string m_sTitle;
    Uint8 m_bgr;
    Uint8 m_bgg;
    Uint8 m_bgb;

    std::atomic<bool> m_bAtomActive;
    std::condition_variable m_cvGameFinished;
    std::mutex m_muxGame;
};

#endif
