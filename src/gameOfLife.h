#include "gameengine.h"
#include <fstream>
#include <random>
#include <time.h>

class GameOfLife : public GameEngine
{
public:
    GameOfLife();
    ~GameOfLife();

private:
    int m_width;
    int m_height;
    int m_stateIndexSize;
    int m_stateIndex;
    short **m_state;
    int m_pixelSize;
    unsigned int m_nCount;
    int m_shiftx;
    int m_shifty;
    int m_cursorx;
    int m_cursory;
    int m_selectx1;
    int m_selecty1;
    int m_selectx2;
    int m_selecty2;
    int m_movex;
    int m_movey;
    enum GAME_STATE { GS_PAUSE, GS_PAUSING, GS_PLAY, GS_STEP_BY_STEP, GS_ASK_LOADING, GS_ASK_SAVING } m_GameState, m_NextState;
    enum USER_MODE { UM_DRAW, UM_SELECT, UM_MOVE, UM_INFO } m_UserMode;
    enum ALGORITHM { A_QUICK, A_REGULAR } m_Algorithm;
    enum BORDER { B_SOLID, B_WRAP } m_Border;
    int m_delay;
    bool m_showGrid;

protected:
    virtual bool onUserCreate();
    virtual bool onUserUpdate(float fElapsedTime);
    void handleEvents(float fElapsedTime);
    void evolveQL();
    void evolveRL();
    void addNeighborhood(int index, int x, int y);
    unsigned int countNeighbor(int index, int x, int y);
    void populateCheckList();
    void show(float fElapsedTime);

    bool save(std::string filename);
    bool load(std::string filename);

    void rotate();
    void move(int movex, int movey);
    void flip();
    void mirror();
    void erase(int xs = 0, int xe = 0, int ys = 0, int ye = 0);

    void random();
};

void listoftwo(short *output, unsigned int size);
void listofzero(short *output, unsigned int size);
void twointozero(short *output, unsigned int size);
void zerointotwo(short *output, unsigned int size);
void printMatrix(short *input, unsigned int width, unsigned int height);
