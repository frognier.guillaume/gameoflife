#include "gameengine.h"

GameEngine::GameEngine()
{
    m_bAtomActive = false;
    m_bRenderNeeded = false;
    m_bFont = false;
    m_windowWidth = 0;
    m_windowHeight = 0;
    m_sTitle = "GF - Game Engine - ";
    m_sAppName = "";
    m_bgr = 0;
    m_bgg = 0;
    m_bgb = 0;
}

GameEngine::~GameEngine()
{
}

void GameEngine::initWindow(int width, int height, bool fullscreen)
{
    m_windowWidth = width;
    m_windowHeight = height;
    m_bAtomActive = false;
    int flags = 0;
    if (fullscreen)
        flags = SDL_WINDOW_FULLSCREEN;
    if (SDL_Init(SDL_INIT_VIDEO) == 0) {
        std::cout << "SDL init." << std::endl;
        m_window = SDL_CreateWindow((m_sTitle + m_sAppName).c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, flags);
        if (m_window) {
            std::cout << "SDL create window." << std::endl;
            m_renderer = SDL_CreateRenderer(m_window, -1, SDL_RENDERER_ACCELERATED);
            if (m_renderer) {
                std::cout << "SDL create render." << std::endl;
            } else {
                fprintf(stderr, "Error SDL_CreateRenderer : %s\n", SDL_GetError());
                exit(EXIT_FAILURE);
            }
        } else {
            fprintf(stderr, "Error SDL_CreateWindow : %s\n", SDL_GetError());
            exit(EXIT_FAILURE);
        }
        m_bAtomActive = true;
    } else {
        fprintf(stderr, "Error SDL_Init : %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }
}

void GameEngine::initText()
{
    if (TTF_Init() == 0) {
#ifdef _WIN32
        const char PATH_SEP = '\\';
#else
        const char PATH_SEP = '/';
#endif
        static std::string fontPath;
        char *basePath = SDL_GetBasePath();
        if (!basePath) {
            std::cerr << "Error getting resource path: " << SDL_GetError() << std::endl;
            return;
        }
        fontPath = basePath;
        SDL_free(basePath);
        fontPath.erase(fontPath.length() - 4); // erase "bin/"
        fontPath = fontPath + "res" + PATH_SEP + "NotoSansMono-Bold.ttf";
        m_font = TTF_OpenFont(fontPath.c_str(), 24);
        if (not m_font) {
            std::cout << "Error Font not found" << std::endl;
        } else {
            m_bFont = true;
        }
    } else {
        fprintf(stderr, "Error TTF_Init : %s\n", TTF_GetError());
        m_bAtomActive = false;
        exit(EXIT_FAILURE);
    }
}

void GameEngine::handleEvents()
{
    SDL_Event event;
    SDL_PollEvent(&event);
    switch (event.type) {
    case SDL_QUIT:
        m_bAtomActive = false;
        break;
    default:
        break;
    }
}

void GameEngine::setBackGroundColor(Uint8 r, Uint8 g, Uint8 b)
{
    m_bgr = r;
    m_bgg = g;
    m_bgb = b;
}

void GameEngine::render()
{
    SDL_RenderPresent(m_renderer);
    m_bRenderNeeded = false;
}

void GameEngine::clearScreen()
{
    SDL_SetRenderDrawColor(m_renderer, m_bgr, m_bgg, m_bgb, SDL_ALPHA_OPAQUE);
    SDL_RenderClear(m_renderer);
}

void GameEngine::clean()
{
    if (m_bFont) {
        TTF_CloseFont(m_font);
        TTF_Quit();
    }
    SDL_DestroyWindow(m_window);
    SDL_DestroyRenderer(m_renderer);
    SDL_Quit();
    std::cout << "Game cleaned." << std::endl;
}

bool GameEngine::running()
{
    return m_bAtomActive;
}

void GameEngine::drawString(std::string sSentence, int x, int y, int size, Uint8 r, Uint8 g, Uint8 b, Uint8 a)
{
    if (!m_bFont)
        return;
    int ndigit = sSentence.length();
    SDL_Color col = {r, g, b, a};
    SDL_Surface *tmpsurface = TTF_RenderText_Solid(m_font, sSentence.c_str(), col);
    SDL_Texture *tmptexture = SDL_CreateTextureFromSurface(m_renderer, tmpsurface);
    SDL_Rect position;
    position.x = x;
    position.y = y;
    position.h = size;
    position.w = size * ndigit;
    SDL_RenderCopy(m_renderer, tmptexture, NULL, &position);
    SDL_DestroyTexture(tmptexture);
    SDL_FreeSurface(tmpsurface);
    m_bRenderNeeded = true;
}

void GameEngine::fillRect(int x, int y, int w, int h, Uint8 r, Uint8 g, Uint8 b, Uint8 a)
{
    if (x + w < 0 or x - w > (int)(m_windowWidth) or y + h < 0 or y - h > (int)(m_windowHeight))
        return;
    SDL_Rect rect;
    rect.x = x;
    rect.y = y;
    rect.w = w;
    rect.h = h;
    SDL_SetRenderDrawColor(m_renderer, r, g, b, a);
    SDL_RenderFillRect(m_renderer, &rect);
    m_bRenderNeeded = true;
}

void GameEngine::drawRect(int x, int y, int w, int h, Uint8 r, Uint8 g, Uint8 b, Uint8 a)
{
    if (x + w < 0 or x - w > (int)(m_windowWidth) or y + h < 0 or y - h > (int)(m_windowHeight))
        return;
    SDL_Rect rect;
    rect.x = x;
    rect.y = y;
    rect.w = w;
    rect.h = h;
    SDL_SetRenderDrawColor(m_renderer, r, g, b, a);
    SDL_RenderDrawRect(m_renderer, &rect);
    m_bRenderNeeded = true;
}

void GameEngine::drawTriangle(int xa, int ya, int xb, int yb, int xc, int yc, Uint8 r, Uint8 g, Uint8 b, Uint8 a)
{
    SDL_SetRenderDrawColor(m_renderer, r, g, b, a);
    SDL_RenderDrawLine(m_renderer, xa, ya, xb, yb);
    SDL_RenderDrawLine(m_renderer, xa, ya, xc, yc);
    SDL_RenderDrawLine(m_renderer, xb, yb, xc, yc);
}

void GameEngine::fillTriangle(int xa, int ya, int xb, int yb, int xc, int yc, Uint8 r, Uint8 g, Uint8 b, Uint8 a)
{
    SDL_SetRenderDrawColor(m_renderer, r, g, b, a);
    SDL_RenderDrawLine(m_renderer, xa, ya, xb, yb);
    SDL_RenderDrawLine(m_renderer, xa, ya, xc, yc);
    SDL_RenderDrawLine(m_renderer, xb, yb, xc, yc);
    /*
    for (int x=xb; x<=xc; x++){
        for (int y=yb; y<=yc; y++){
            SDL_RenderDrawLine(m_renderer,xa,ya,x,y);
        }
    }
    */
}

void GameEngine::drawGrid(int xs, int ys, int w, int h, int sizex, int sizey, Uint8 r, Uint8 g, Uint8 b, Uint8 a)
{
    if (xs + w < 0 || xs - w > (int)(m_windowWidth) || ys + h < 0 || ys - h > (int)(m_windowHeight) || sizex <= 2 || sizey <= 2)
        return;
    SDL_SetRenderDrawColor(m_renderer, r, g, b, a);
    for (int y = ys; y < ys + h; y += sizey) {
        SDL_RenderDrawLine(m_renderer, xs, y, xs + w, y);
    }
    for (int x = xs; x < xs + w; x += sizex) {
        SDL_RenderDrawLine(m_renderer, x, ys, x, ys + h);
    }
    m_bRenderNeeded = true;
}

void GameEngine::setWindowTitle(unsigned fps)
{
    SDL_SetWindowTitle(m_window, (m_sTitle + std::to_string(fps)).c_str());
}

void GameEngine::setWindowTitle()
{
    SDL_SetWindowTitle(m_window, (m_sTitle + m_sAppName).c_str());
}

void GameEngine::GameThread()
{
    if (!onUserCreate())
        m_bAtomActive = false;

    Uint64 start = SDL_GetPerformanceCounter();
    Uint64 end = SDL_GetPerformanceCounter();

    // Run as fast as possible
    while (m_bAtomActive) {
        // handle events
        // handleEvents();
        end = SDL_GetPerformanceCounter();
        const static Uint64 freq = SDL_GetPerformanceFrequency();
        float fElapsedTime = (end - start) / static_cast<double>(freq); // sec
        start = end;
        if (!onUserUpdate(fElapsedTime))
            m_bAtomActive = false;
        // unsigned fps = std::floor(1.0f / fElapsedTime);
        // setWindowTitle(fps);
        if (m_bRenderNeeded)
            render();
    }
    clean();
    m_cvGameFinished.notify_one();
}

void GameEngine::start()
{
    // Star the thread
    std::thread t(&GameEngine::GameThread, this);

    // Wait for thread to be exited
    std::unique_lock<std::mutex> ul(m_muxGame);
    m_cvGameFinished.wait(ul);

    // Tidy up
    t.join();
}

void GameEngine::end()
{
    m_bAtomActive = false;
}

int GameEngine::windowWidth() const
{
    return m_windowWidth;
}

int GameEngine::windowHeight() const
{
    return m_windowHeight;
}

std::string GameEngine::askBox(int x, int y, int w, int h)
{
    if (not m_bFont)
        return "Enable Font";
    std::string sString = "";
    SDL_Rect rect;
    rect.x = x;
    rect.y = y;
    rect.w = w;
    rect.h = h;
    SDL_SetRenderDrawColor(m_renderer, 255, 255, 255, 255);
    SDL_RenderFillRect(m_renderer, &rect);
    render();
    while (m_bAtomActive) {
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
            case SDL_QUIT:
                m_bAtomActive = false;
                break;
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym) {
                case SDLK_ESCAPE:
                    m_bAtomActive = false;
                    break;
                case SDLK_a:
                    sString.append("a");
                    m_bRenderNeeded = true;
                    break;
                case SDLK_b:
                    sString.append("b");
                    m_bRenderNeeded = true;
                    break;
                case SDLK_c:
                    sString.append("c");
                    m_bRenderNeeded = true;
                    break;
                case SDLK_d:
                    sString.append("d");
                    m_bRenderNeeded = true;
                    break;
                case SDLK_e:
                    sString.append("e");
                    m_bRenderNeeded = true;
                    break;
                case SDLK_f:
                    sString.append("f");
                    m_bRenderNeeded = true;
                    break;
                case SDLK_g:
                    sString.append("g");
                    m_bRenderNeeded = true;
                    break;
                case SDLK_h:
                    sString.append("h");
                    m_bRenderNeeded = true;
                    break;
                case SDLK_i:
                    sString.append("i");
                    m_bRenderNeeded = true;
                    break;
                case SDLK_j:
                    sString.append("j");
                    m_bRenderNeeded = true;
                    break;
                case SDLK_k:
                    sString.append("k");
                    m_bRenderNeeded = true;
                    break;
                case SDLK_l:
                    sString.append("l");
                    m_bRenderNeeded = true;
                    break;
                case SDLK_m:
                    sString.append("m");
                    m_bRenderNeeded = true;
                    break;
                case SDLK_n:
                    sString.append("n");
                    m_bRenderNeeded = true;
                    break;
                case SDLK_o:
                    sString.append("o");
                    m_bRenderNeeded = true;
                    break;
                case SDLK_p:
                    sString.append("p");
                    m_bRenderNeeded = true;
                    break;
                case SDLK_q:
                    sString.append("q");
                    m_bRenderNeeded = true;
                    break;
                case SDLK_r:
                    sString.append("r");
                    m_bRenderNeeded = true;
                    break;
                case SDLK_s:
                    sString.append("s");
                    m_bRenderNeeded = true;
                    break;
                case SDLK_t:
                    sString.append("t");
                    m_bRenderNeeded = true;
                    break;
                case SDLK_u:
                    sString.append("u");
                    m_bRenderNeeded = true;
                    break;
                case SDLK_v:
                    sString.append("v");
                    m_bRenderNeeded = true;
                    break;
                case SDLK_w:
                    sString.append("w");
                    m_bRenderNeeded = true;
                    break;
                case SDLK_x:
                    sString.append("x");
                    m_bRenderNeeded = true;
                    break;
                case SDLK_y:
                    sString.append("y");
                    m_bRenderNeeded = true;
                    break;
                case SDLK_z:
                    sString.append("z");
                    m_bRenderNeeded = true;
                    break;
                case SDLK_SPACE:
                    sString.append(" ");
                    m_bRenderNeeded = true;
                    break;
                    break;
                case SDLK_1:
                case SDLK_KP_1:
                    sString.append("1");
                    m_bRenderNeeded = true;
                    break;
                case SDLK_2:
                case SDLK_KP_2:
                    sString.append("2");
                    m_bRenderNeeded = true;
                    break;
                case SDLK_3:
                case SDLK_KP_3:
                    sString.append("3");
                    m_bRenderNeeded = true;
                    break;
                case SDLK_4:
                case SDLK_KP_4:
                    sString.append("4");
                    m_bRenderNeeded = true;
                    break;
                case SDLK_5:
                case SDLK_KP_5:
                    sString.append("5");
                    m_bRenderNeeded = true;
                    break;
                case SDLK_6:
                case SDLK_KP_6:
                    sString.append("6");
                    m_bRenderNeeded = true;
                    break;
                case SDLK_7:
                case SDLK_KP_7:
                    sString.append("7");
                    m_bRenderNeeded = true;
                    break;
                case SDLK_8:
                case SDLK_KP_8:
                    sString.append("8");
                    m_bRenderNeeded = true;
                    break;
                case SDLK_9:
                case SDLK_KP_9:
                    sString.append("9");
                    m_bRenderNeeded = true;
                    break;
                case SDLK_0:
                case SDLK_KP_0:
                    sString.append("0");
                    m_bRenderNeeded = true;
                    break;
                case SDLK_MINUS:
                case SDLK_KP_MINUS:
                    sString.append("-");
                    m_bRenderNeeded = true;
                    break;
                case SDLK_BACKSPACE:
                    if (!sString.empty()) {
                        sString.pop_back();
                        m_bRenderNeeded = true;
                    }
                    break;
                case SDLK_RETURN:
                case SDLK_KP_ENTER:
                    return sString;
                default:
                    break;
                }
            default:
                break;
            }
        }
        if (m_bRenderNeeded) {
            SDL_SetRenderDrawColor(m_renderer, 255, 255, 255, 255);
            SDL_RenderFillRect(m_renderer, &rect);

            drawString(sString, x + 1, y, h, 0, 0, 0, 255);

            render();
        }
    }
    return "";
}
