CC := g++
SRCDIR := src
BUILDDIR := build
TARGET := bin/program

CFLAGS := -Wall -Wextra -std=c++17
LFLAGS := -lSDL2 -lSDL2_ttf

SRCEXT := cpp
SOURCES := $(wildcard ${SRCDIR}/*.$(SRCEXT))
OBJECTS := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.$(SRCEXT)=.o))
DEP := $(OBJECTS:.o=.d)

$(TARGET): $(OBJECTS)
	@mkdir -p bin
	@echo " Linking..."
	@echo " $(CC) $^ -o $(TARGET) $(LFLAGS)"; $(CC) $^ -o $(TARGET) $(LFLAGS)

$(BUILDDIR)/%.o: $(SRCDIR)/%.$(SRCEXT)
	@mkdir -p $(dir $@)
	@echo " $(CC) $(CFLAGS) -MMD -c -o $@ $<"; $(CC) $(CFLAGS) -MMD -c -o $@ $<

-include $(DEP)

.PHONY: clean
clean:
	@echo " Cleaning...";
	@echo " $(RM) -r $(BUILDDIR) $(TARGET)"; $(RM) -r $(BUILDDIR) $(TARGET)

.PHONY: debug
debug: CFLAGS += -g -DDEBUG
debug: $(TARGET)

.PHONY: release
release: CFLAGS += -O2
release: $(TARGET)
